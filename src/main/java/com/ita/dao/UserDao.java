package com.ita.dao;

import com.ita.entity.User;

import java.util.List;

public interface UserDao {

    void save(User user);

    User getById(int id);

    List<User> findAll();

    void update(User user);

    void deleteById(int id);
}
