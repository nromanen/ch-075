package com.ita.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.ita.constant.STR_CONSTANT.PROPERTIES_PATH;

public class PropertiesUtil {

    private static final Properties properties = loadProperties();

    private static Properties loadProperties(){
        Properties property = new Properties();
        try {
            InputStream inputStream =
                    PropertiesUtil.class.getClassLoader().getResourceAsStream(PROPERTIES_PATH);
            property.load(inputStream);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return property;
    }

    public static String getProperty(String key){
        return properties.getProperty(key);
    }
}