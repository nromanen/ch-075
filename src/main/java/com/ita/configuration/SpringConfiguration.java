package com.ita.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

import static com.ita.utility.PropertiesUtil.getProperty;

@Configuration
@ComponentScan(basePackages = {"com.ita.service", "com.ita.dao"})
public class SpringConfiguration {

    @Bean
    public JdbcTemplate getJdbcTemplate(){
        return new JdbcTemplate(getDataSource());
    }

    @Bean
    public DataSource getDataSource(){

        DriverManagerDataSource driver = new DriverManagerDataSource();
        driver.setUrl(getProperty("db.url"));
        driver.setUsername(getProperty("db.username"));
        driver.setPassword(getProperty("db.password"));
        driver.setDriverClassName(getProperty("db.driverName"));

        return driver;
    }
}
