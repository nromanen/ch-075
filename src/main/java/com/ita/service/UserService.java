package com.ita.service;

import com.ita.entity.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    void save(User user);

    User getById(int id);

    void update(User user);

    void deleteById(int id);
}
